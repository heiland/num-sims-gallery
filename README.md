## Double Cylinder

 * Features
   * uncertain parameters in 4 domains
 * Paper: Benner, Heiland, Werner: [tba](tba)
 * Code: [github.com/jnhlnd/dolfin_navier_scipy](https://github.com/jnhlnd/dolfin_navier_scipy)

![velocity for `Re=50`, at `t=80` started from steady state Stokes](double-rotcyl/dbrc-v_Re50_t80_cm-bbw.png)
![velocity for `Re=50`, at `t=669` started from steady state Stokes](double-rotcyl/dbrc-v_Re50_t669_cm-ctw-ext.png)
![velocity for `Re=50`, at steady state](double-rotcyl/dbrc-v_Re50_stst_cm-bbw+legend.png)
![pressure for `Re=50`, at `t=80` started from steady state Stokes](double-rotcyl/dbrc-p_Re50_t80_cm-rb-des.png)
![pressure for `Re=50`, at `t=80` started from steady state Stokes](double-rotcyl/dbrc-p_Re50_stst_cm-rb-des+legend.png)
[video](double-rotcyl/animation-dbrc-v-ctw-ext/Re50-t0t600.avi)
[video](double-rotcyl/animation-dbrc-v-ctw-ext/Re50-t0t600.mp4)

## Convection-diffusion on 3D Doughnut

![Example Solution](3D-doughnut/conv-diff-3D-doughnut-N7-sol.png)
![Difference in the expected value computed with PCE and a genpod approximation](3D-doughnut/conv-diff-3D-doughnut-N7-pcepoddiff-Expval.png)

 * Features
   * uncertain parameters in 4 domains
 * Paper: Benner, Heiland: [*Space and Chaos-Expansion Galerkin POD Low-order Discretization of PDEs for Uncertainty Quantification*](https://arxiv.org/abs/2009.01055)
 * Code: [github.com/mpimd-csc/multidim-genpod-uq](https://github.com/mpimd-csc/multidim-genpod-uq)
